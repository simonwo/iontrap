// Graphics functions
// Simon Worthington 2011
#include "SDL.h"
#include <stdio.h>
using namespace std;

/*
	N.B. These don't update the screen, so remember
	to call SDL_UpdateRect at an appropriate time!
*/

// Standard 32-bit set pixel routine 
void DrawPixel(SDL_Surface *surface, int x, int y, Uint32 pixel) {
	if (x < surface->w && y < surface->h && y > 0 && x > 0) {
		Uint8 *target_pixel = (Uint8 *)surface->pixels + y * surface->pitch + x * 4;
		*(Uint32 *)target_pixel = pixel;
	}
} 

// Draw a rectangle of the specified dimensions
void DrawRectangle(SDL_Surface *surface, int x, int y, int width, int height, Uint32 pixel) {
	for (int i = x; i <= width + x; i++) {
		for (int j = y; j <= height + y; j++) {
			DrawPixel(surface, i, j, pixel);
		}
	}
}

// Fill the screen with a colour
void FillScreen(SDL_Surface *surface, Uint32 pixel) {
	DrawRectangle(surface, 0, 0, surface->w, surface->h, pixel);
} 