#include "SDL.h"
#include "Graphics.h"
#include <fstream>
#include <sstream>
#include <string>
#include <vector>

#define PI acos(-1.0)

void DrawGrid(SDL_Surface* screen, const int spacing, Uint32 colour) {
	for (int i = 0; i <= screen->w; i+= spacing) {
		DrawRectangle(screen, i, 0, 1, screen->h, colour);
		DrawRectangle(screen, 0, i, screen->w, 1, colour);
	}
}

int main( int argc, char* argv[] )
{
	const int screenw = 800, screenh = 800;

	vector<double> omega(argc-2);
	for (int i = 2; i < argc; i++) {
		omega[i-2] = std::atof(argv[i]);
	}

	SDL_Init( SDL_INIT_VIDEO );
	SDL_WM_SetCaption(argc > 1 ? argv[1] : "results.txt", "");
	SDL_Surface* screen = SDL_SetVideoMode(screenw, screenh, 32, SDL_SWSURFACE);
	if ( screen == NULL ) {
	fprintf(stderr, "Couldn't set 640x480x8 video mode: %s\n", SDL_GetError());
	exit(1);
	}

	
	std::ifstream file(argc > 1 ? argv[1] : "results.txt");
	double traph = 100E-6, trapw = 100E-6;
	double t, x, y, z;
	bool trails = true;

	SDL_Event Event;
	std::string line;

	DrawGrid(screen, 8, SDL_MapRGB(screen->format, 32, 32, 32));

	while (std::getline(file, line)) {
		if (SDL_PollEvent(&Event)) {
			if (Event.type == SDL_QUIT)
				return 0;
			if (Event.type == SDL_KEYDOWN) {
				switch(Event.key.keysym.sym){
					case SDLK_UP:
						omega[0] += 1E5;
						break;
					case SDLK_DOWN:
						omega[0] -= 1E5;
						break;
					case SDLK_LEFT:
						omega[0] -= 1E3;
						break;
					case SDLK_RIGHT:
						omega[0] += 1E3;
						break;
					case SDLK_SPACE:
						trails = !trails;
						break;
					default:
						FillScreen(screen, 0);
						DrawGrid(screen, 8, SDL_MapRGB(screen->format, 32, 32, 32));
				}
			}
		}

		std::istringstream iss(line);
		iss >> t;

		int particle = 0;

		if (!trails) {
			FillScreen(screen, 0);
			DrawGrid(screen, 8, SDL_MapRGB(screen->format, 32, 32, 32));
		}

		// Auto derect particles
		while (iss >> x >> y >> z) {
			particle++;
			
			if (omega.size() != 0) {
				// Convert to polar coords, apply rotating frame, convert back
				double theta = atan2(y, x);
				double r = sqrt(x*x + y*y);
				for (vector<double>::iterator w = omega.begin(); w != omega.end(); w++) {
					theta = std::fmod( theta + std::fmod(*w * t, 2 * PI), 2 * PI);
				}
				x = r * cos(theta);
				y = r * sin(theta);
			}

			if (trails) {
				DrawPixel(screen, (int)(screenw * (x/trapw) + screenw/2),  
							  (int)(screenh * (y/traph) + screenh/2), 
							  SDL_MapRGB(screen->format, (particle == 1 ? 255 : 0), (particle == 2 ? 255 : 0), (particle == 3 ? 255 : 0)));
			} else {
				DrawRectangle(screen, (int)(screenw * (x/trapw) + screenw/2 + 1),  
									  (int)(screenh * (y/traph) + screenh/2 + 1),
									  2, 2,
									  SDL_MapRGB(screen->format, (particle == 1 ? 255 : 0), (particle == 2 ? 255 : 0), (particle == 3 ? 255 : 0)));
			}
		}
		SDL_UpdateRect(screen, 0, 0, screenw, screenh);
	}

	return 0;
}