#include "particle.cpp"
#include "vector3D.cpp"
#include <cmath>
#pragma once

class Force {

public:
        // Returns the force to be applied to the particle.
        virtual vector3D<double> ApplyTo(const Particle&) {
                return vector3D<double>();
        };
        virtual const double FieldStrength(){
                return 0.0;             
                };
        virtual const double MagStrength(){
                return 0.0;             
                };
                
                virtual double PotentialEnergy(const Particle& particle){
                return 0.0;                     
                        }
};


// This generates a static, quadrupole electric field. Potential is governed by V = A(2*z^2 - r^2)
class ElectricField : public Force {
        private:
                //Trap potential parameter is given by A 
                double A;
        public:
                ElectricField(double strength){
                        A = strength;
                }

                vector3D<double> ApplyTo(const Particle& particle) {
                        vector3D<double> force(0,0,0);
                        // Logic for working out the actual force goes here
                        // e.g. particle->position->x + particle->position->y etc

                        force = particle.position * 2.0 * A * (1.6E-19) * particle.charge;
                        force.z *=-2.0;
                        return force;
                }

                const double FieldStrength() {
                        return A;
                }

};

//This class defines a static, uniform magnetic field vector
class MagneticField : public Force {
        private:
                vector3D<double> fieldvector;

        public:
                //Default constructor will create a field with no field strength
                MagneticField() {
                        fieldvector = vector3D<double>(0, 0, 0);
                }

                //This constructs a magnetic field using a vector
                MagneticField(vector3D<double> field){
                        fieldvector = field;
                }

                //Creates a field vector out of Cartesian coordinates
                MagneticField(double a, double b, double c) {
                        fieldvector.x = a;
                        fieldvector.y = b;
                        fieldvector.z = c;
                }

                vector3D<double> ApplyTo(const Particle &particle){
                        vector3D<double> force(0,0,0);
                        // I don't quite know how vector cross products will work with this syntax, but here goes (note ^ is operator used for cross product)
                        force = ((particle.velocity)^fieldvector)*(1.6E-19)*particle.charge;
                        //I put the scalar product at the end, in case the order matters

                        return force;
                }


                const vector3D<double> FieldVector() {
                        return fieldvector;
                }
                
                const double MagStrength(){
                                return fieldvector.mag();                       
                        }
};

class CoulombForce : public Force {
        private:
                Particle* p;

        public:
                CoulombForce(Particle* p) : p(p) {};

                vector3D<double> ApplyTo(const Particle& particle) {
                        vector3D<double> force = vector3D<double>(0,0,0);
                        
                        // Only calculate a force if the two particles are different.
                        if (*p != particle) { 
                                // f = q1q2 / 4pi e0 r^2 in direction of -r (r - from p to particle) (particle.position - p.position)
                                vector3D<double> r = particle.position - p->position;
                                force = r * (p->charge * particle.charge * (1.6E-19) * (1.6E-19)/ (4 * 3.14159 * 8.85E-12 * pow(r.mag(), 3)));
                        }
                        return force;
                };

                double PotentialEnergy(const Particle& particle) {
                        if (*p == particle) {
                                return 0.0;
                        } else {
                                return p->charge * 1.6E-19 * particle.charge * 1.6E-19 / (4 * 3.14159 * 8.85E-12 * (particle.position - p->position).mag());
                        }
                }
};

class Laser : public Force {
public:
        double fudgefactor;
        double width;
        // Position can be given by an arbitrary point the centre of the laser passes through
        vector3D<double> position;
        // Direction is the vector along which the laser is pointing
        vector3D<double> direction;

        double wavelength;      // Wavelength given in metres
        double detuning;        // How much the laser is detuned, in Hz
		double saturation;

        // Constructor for the laser
        Laser(double a, double b, vector3D<double> c, vector3D<double> d){
                fudgefactor = a;
                width = b;
                position = c;
                direction = d/d.mag();
        }

        Laser(double a, double b, double c, double d, double e, double f, double g, double h){
                fudgefactor = a;
                width = b;
                position = vector3D<double>(c,d,e);
                direction = vector3D<double>(f,g,h);
				direction = direction / direction.mag();
        }

        //Intensity calculation
        double Distancefactor(vector3D<double> point){
                // First, find point on line closest to point given
                // Closepoint
                double lambda = direction * (point - position);
                vector3D<double> closepoint = position + direction*lambda;
                // Now, find distance between point and closest point on line
                double distance = (point-closepoint).mag();
                return exp(-distance*distance/(width*width));
        }

        // Calculating the (non-relativistic) Doppler effect

        double DopplerDetune(vector3D<double> vel){
                // Velocity component in direction into laser is calculated
                double veltosrc = -(vel * direction);
                return detuning*(1+veltosrc/3E8)+veltosrc/wavelength;
        }

		double Fluorescence(const Particle& particle) {
			    // The following constants should probably be a part of the particle, rather than just constants
                const double linewidth = 20E6;

                const double distfact = Distancefactor(particle.position);
                const double dopplerdetune = 2*DopplerDetune(particle.velocity)/linewidth;

                return linewidth*saturation*distfact/(2*(1 + saturation*distfact + dopplerdetune*dopplerdetune));
		}

        vector3D<double> ApplyTo(const Particle& particle){
                const double forcemag = fudgefactor*Fluorescence(particle)*6.63E-34/wavelength;
                const vector3D<double> force = direction*forcemag;

                return force;
        }

        double PotentialEnergy(const Particle& particle) {
                return 0.0;
        }
};