#include "Trap.h"
#pragma once

Trap::Trap(double dt) : dt(dt) { };

const double inline RKFactor(int i) {
	return (i == 4) ? 1.0 : ((i== 1) ? 0.0 : 0.5);
}

void Trap::Simulate() {

	// Reset the original velocity and position so we can restore it later
	for (std::vector<Particle*>::iterator p = particles.begin(); p != particles.end(); p++) {
		(**p).vk[0] = (**p).velocity;
		(**p).ak[0] = (**p).position; // using ak for position just to save variables
	}

	for (int i = 1; i < 5; i++) {
		
		// Calculate all the new positions and velocities first
		for (std::vector<Particle*>::iterator p = particles.begin(); p != particles.end(); p++) {

			// Set (**p) velocity and position
			(**p).position = (**p).ak[0] + (**p).vk[i-1] * RKFactor(i) * dt;
			(**p).velocity = (**p).vk[i] = (**p).vk[0] + (**p).ak[i-1] * RKFactor(i) * dt;

		}

		// Now apply the forces for each particle
		for (std::vector<Particle*>::iterator p = particles.begin(); p != particles.end(); p++) {
			vector3D<double> force(0,0,0); 
			for (std::vector<Force*>::iterator f = forces.begin(); f != forces.end(); f++) {
				force += (*f)->ApplyTo((**p));
			}
			(**p).ak[i] = force / (**p).mass;
		}
	}

	// Apply the results of simulation
	for (std::vector<Particle*>::iterator p = particles.begin(); p != particles.end(); p++) {
		(**p).velocity = (**p).vk[0] + ((**p).ak[1] + (**p).ak[2] * 2 + (**p).ak[3] * 2 + (**p).ak[4]) * dt / 6;
		(**p).position = (**p).ak[0] + ((**p).vk[1] + (**p).vk[2] * 2 + (**p).vk[3] * 2 + (**p).vk[4]) * dt / 6;
	}
};

bool Trap::IsInEquilibrium() const {
	return false; // meaningless
};

double Trap::OnePeriod(std::vector<Calculator*>& calculators) {
	Particle* ion = this->particles[0];
	int count = 0;
	vector3D<double> CoM(0,0,0);
	for (std::vector<Particle*>::iterator p = this->particles.begin(); p != this->particles.end(); p++) {
		count++;
		CoM += (*p)->position;
		
	}

	CoM = CoM / count;
	
	vector3D<double> relpos;

	for (std::vector<Particle*>::iterator p = this->particles.begin(); p != this->particles.end(); p++) {
		relpos = (*p)->position - CoM;
		if (relpos.radius() > (ion->position - CoM).radius()) ion = *p;
	}	
	
	double time = 0;
	bool half = false;
	
	// Find original angle of particle
	relpos = ion->position - CoM;
	double angle = relpos.azimuth();
	double relangle = 0;
	while( (relangle > 0.001 && relangle - 2*acos(-1.0) < 0.001) || half == false){
		this->Simulate();
		count = 0;
		CoM = vector3D<double>(0,0,0);
		for (std::vector<Particle*>::iterator p = this->particles.begin(); p != this->particles.end(); p++) {
			count++;
			CoM += (*p)->position;
		}
		CoM = CoM / count;
		relpos = ion->position - CoM;
		relangle = fmod(relpos.azimuth() - angle, 2*acos(-1.0));
		if(relangle < 0) relangle += 2*acos(-1.0);
		time += dt;
		
		
		if( fabs(relangle - acos(-1.0)) < 0.001){
			half = true;
		}
	
		for (std::vector<Calculator*>::iterator c = calculators.begin(); c != calculators.end(); c++) {
				for (std::vector<Particle*>::iterator p = this->particles.begin(); p != this->particles.end(); p++) {
					(**c).Calculate(**p);
				}
			}
		
	}

	return time;
	
		/*// Establish x direction
		double x = ion->position.x;
		do { this->Simulate(); } while (x == ion->position.x);
		compare = (ion->position.x > x ? &gt : &lt);
	
		// Carry on simulating until x crosses the original value (on other side of circle).
		// Then, wait for it to go past our original value. That's one full rotation.
		bool swapped = false;
		while (!swapped || (swapped && (*compare)(ion->position.x, x))) {
			this->Simulate();
			time += dt;
	
			for (std::vector<Calculator*>::iterator c = calculators.begin(); c != calculators.end(); c++) {
				for (std::vector<Particle*>::iterator p = this->particles.begin(); p != this->particles.end(); p++) {
					(**c).Calculate(**p);
				}
			}
	
			// If this is taking too long, we may be in an infinite loop. Rather than fix this, let's do:
			if (time > 0.05) return this->OnePeriod(calculators);
	
			// If comparison no longer true, swap the comparison, but only do this once
			if (!swapped && !(*compare)(ion->position.x, x)) {
				compare = (compare == &gt ? &lt : &gt);
				swapped = true;
			}
		}*/
	
	// The center position is now the average of all the points (assumes we are moving in a circle).
}

Trap::~Trap(void)
{
};
/*
vk1 = (force(y) / mass) * dt;
vk2 = (force(y + 0.5k1) / mass) * dt;
vk3 = (force(y + 0.5k2) / mass) * dt;
vk4 = (force(y + k3) / mass) * dt;
v += (k1 + 2k2 + 2k3 + k4) / 6 */



// Assuming the trap is quadrupole and the magnetic field is aligned along the positive z-axis. Field is static
double Trap::PotentialEnergy(){
	double A = 0.0;
	double B = 0.0;
	double PotEn = 0.0;
	for (std::vector<Force*>::iterator i = forces.begin(); i != forces.end(); i++){
			A += (*i)->FieldStrength();
			B += (*i)->MagStrength();
		}
		
	for (std::vector<Particle*>::iterator i = particles.begin(); i != particles.end(); i++){
			double partcharge = (*i)->charge*1.6E-19;
			// Adds the static field potential of a particle
			PotEn += partcharge * (partcharge * B * B / (8*(*i)->mass) - A) * (pow((*i)->position.x,2)+pow((*i)->position.y,2)) + 2*A*partcharge*pow((*i)->position.z,2);
			// Coulomb interaction potential due to the presence of several particles
			// This might not be the most efficient way of working this out,
			// but it's the only way I can think of right now
			for (std::vector<Force*>::iterator f = forces.begin(); f != forces.end(); f++){
				PotEn += (*f)->PotentialEnergy(**i);
			}
	}
	return PotEn;
}

double Trap::SingleParticlePotential(Particle& part){
	double A = 0.0;
	double B = 0.0;
	for (std::vector<Force*>::iterator i = forces.begin(); i != forces.end(); i++){
			A += (*i)->FieldStrength();
			B += (*i)->MagStrength();
		}
		double partcharge = part.charge*1.6E-19;
		return partcharge * (partcharge * B * B / (8*part.mass) - A) * (pow(part.position.x,2)+pow(part.position.y,2)) + 2*A*partcharge*pow(part.position.z,2);
}

void Trap::Randomiser(Particle& part, double temperature, bool flat){
	// Value stays false until acceptable value for the energy has been found		
			bool value = false;
			double energy = 0;
			while(value==false){
				// Find random energy
				energy = std::rand()/(RAND_MAX*1E20);
				// Use rejection method to check if energy is acceptable
				double prob = sqrt(2*energy/(1.38E-23*temperature))*exp(0.5-energy/(1.38E-23*temperature));
				double test = std::rand()*1.0/RAND_MAX;
				if(test<prob){
					value = true;	
				}	
			}
			
			double poten = 1E50;
			
			// Find a random position for the particle, whose position does not have a larger potential energy
			// than the energy of the particle due to its temperature
			while(energy-poten<0){
				part.position.x = (std::rand()/(0.5*RAND_MAX)-1)*1E-3;
				part.position.y = (std::rand()/(0.5*RAND_MAX)-1)*1E-3;
				part.position.z = flat? 0 : (std::rand()/(0.5*RAND_MAX)-1)*1E-3;
				
				poten = SingleParticlePotential(part);
				}
				
				
				// Calculate kinetic energy as total energy minus potential energy				
				
				energy = energy - poten;
				
				// Calculate velocity from kinetic energy
				double vel = sqrt(2*energy/part.mass);
					
			
				// Random direction of velocity is found
				vector3D<double> dir(std::rand()*1.0/RAND_MAX - 0.5, std::rand()*1.0/RAND_MAX - 0.5, flat? 0 : std::rand()*1.0/RAND_MAX - 0.5);
				part.velocity = dir*vel/dir.mag();
		}
