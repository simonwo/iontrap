from subprocess import Popen,PIPE
import os
import time
import multiprocessing
import re 
from threading import Thread
from datetime import datetime
from urllib2 import urlopen, URLError
from urllib import urlencode
from Queue import Queue;

path = r".\Release\IonTrap.exe"
url = "http://iontrap.simonwo.net"
magicword = "a5prus10"

regex = re.compile("(.*): (.*)")

cores = multiprocessing.cpu_count()
processes = []

headers = []

def timestring():
    return datetime.now().time().strftime("%H:%M:%S")

class NonBlockingProcess():
    def __init__(self, process):
        self.process = process
        self.queue = Queue()
        self._thread = Thread(target=self._read)
        self._thread.daemon = True
        self._thread.start()

    def _read(self):
        for line in self.process.stdout:
            self.queue.put(line)
        self.process.stdout.close()

    @property
    def pid(self):
        return self.process.pid

    def poll(self):
        return self.process.poll()

    @property
    def stdout(self):
        while not self.queue.empty():
            yield self.queue.get_nowait()

# Generate jobs 
jobs = []
for potential in xrange(40, 120, 20):
    for detuning in 1,2,3,4,6,10,20,40,60,80:
        for offset in 40, 60:
           job = {'--particles': '4', '--alias': '"Potential v detuning 3"', '--maxtime': '1'}
           job['--detuning'] = '-%iE6' % detuning
           job['--potential'] = '%iE3' % potential
           job['--offset'] = '%iE-6' % offset
           jobs.append(job)
print "[%s] Generated %i jobs" % (timestring(), len(jobs))


while len(jobs) > 0 or len(processes) > 0:
    # Start new job if we have an available core
    while len(jobs) > 0 and len(processes) < cores:
        args = [path];
        job = jobs.pop();
        for argk, argv in job.items():
            args.append(argk)
            args.append(argv)
        pp = NonBlockingProcess(Popen(args, stdout=PIPE))
        print "[%s] Started job %d with parameters" % (timestring(), pp.pid), job
        processes.append(pp);

    # Read from STDOUT and remove completed processes
    print "[%s] %i jobs remaining...\r" % (timestring(), len(jobs)),
    for process in processes:
        if process.poll() is not None:
            
            # Process stdout
            output = {}
            for line in process.stdout:
                if regex.match(line):
                    data = regex.search(line).groups()
                    output[data[0].strip()] = data[1].strip()
            
            with open("collected-data.csv", "a") as f:
                # Assumes each instance outputs the same headers!
                if len(headers) == 0:
                    headers = sorted(output.keys())
                    for h in headers:
                        f.write(h + ", ")
                    f.write('\n')
                
                for h in headers:
                    f.write(output[h] + ", ")
                f.write('\n')
                
            # Upload data
            output["Magic word"] = magicword;
            try:
                urlopen(url, urlencode(output));
                print "[%s] Uploaded results from job %d to" % (timestring(), process.pid), url
            except Exception as e:
                print "[%s] Failed to upload job %d:"% (timestring(), process.pid), e
                    
            
            print "[%s] Job %d finished" % (timestring(), process.pid)
            processes.remove(process)
    
    time.sleep(5)
            