#include "calculators.h"
#include <map>
#pragma once

CentreOfRotationCalculator::CentreOfRotationCalculator(): count(0) {};
void CentreOfRotationCalculator::Calculate(Particle& particle) {
	average += particle.position;
	count++;
}

vector3D<double> CentreOfRotationCalculator::FinalResult() {
	return average / count;
}

AverageFlourescenceCalculator::AverageFlourescenceCalculator(Laser* L): count(0), average(0.0), laser(L) {};
void AverageFlourescenceCalculator::Calculate(Particle& particle) {
	average += laser->Fluorescence(particle);
	count++;
}

double AverageFlourescenceCalculator::FinalResult() {
	return count == 0? 0 : average/count;
}

const double PI = acos(-1.0);
const inline double dtheta(double before, double after) {
	if (before > PI/2.0 && after < -PI/2.0) return (PI - before) + (PI + after);
	if (before < -PI/2.0 && after > PI/2.0) return -((PI + before) + (PI - after));
	return after - before;
}

RotationSpeedCalculator::RotationSpeedCalculator(vector3D<double> centre, double dt): count(0), dt(dt), centre(centre) {
	particles = std::map<int, double>();
}
void RotationSpeedCalculator::Calculate(Particle& particle) {
	double theta = (particle.position - centre).azimuth();
	if (particles.count(particle.id) == 0) {
		particles.insert(std::pair<int, double>(particle.id, 0.0));
	} else {
		particles[particle.id] += dtheta(particle.theta, theta);
	}
	particle.theta = theta;
	count++;
}
double RotationSpeedCalculator::FinalResult(int ParticleID) {
	return particles[ParticleID] / (dt * (count / particles.size()));
}