#include "vector3D.cpp"
#include <random>
#pragma once

struct Particle {

		int id;
public:
		Particle() {
			id = std::rand();
		}

		vector3D<double> position; 
		vector3D<double> velocity;
		double mass;
		int charge;

		// RK4 scratch space for this particle
		vector3D<double> vk[5];
		vector3D<double> ak[5];

		// Theta-dot scratch space
		double theta;

		const bool operator==(const Particle& p) const {
			return (id == p.id);
		}

		const bool operator!=(const Particle& p) const {
			return !(*this == p);
		}
};

	
