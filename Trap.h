#include <list>
#include "forces.cpp"
#include "calculators.h"
#pragma once

class Trap
{
private:
	double dt;

public:
	Trap(double);
	~Trap(void);

	// All the different forces present in the trap
	std::vector<Force*> forces;

	// The particles in the trap
	std::vector<Particle*> particles;

	// Perform a time step, applying all the forces to the particles
	void Simulate();

	bool IsInEquilibrium() const;

	double OnePeriod(std::vector<Calculator*>&);
	
	double PotentialEnergy();
	
	double SingleParticlePotential(Particle&);
	
	void Randomiser(Particle&, double, bool);
};
