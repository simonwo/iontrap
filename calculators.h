#include <map>
#pragma once

class Calculator {
public:
	virtual void Calculate(Particle&) = 0;
};

class CentreOfRotationCalculator : public Calculator {

private:
	vector3D<double> average;
	int count;

public:
	CentreOfRotationCalculator();

	void Calculate(Particle&);
	vector3D<double> FinalResult();
};

class AverageFlourescenceCalculator : public Calculator {
private:
	double average;
	int count;
	Laser* laser;

public:
	AverageFlourescenceCalculator(Laser* L);

	void Calculate(Particle&);
	double FinalResult();
};

class RotationSpeedCalculator : public Calculator {
private:
	std::map<int, double> particles;
	int count;
	double dt;
	vector3D<double> centre;

public:
	RotationSpeedCalculator(vector3D<double>, double);

	void Calculate(Particle&);
	double FinalResult(int);
};