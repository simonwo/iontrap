#ifdef __unix__
        #include "Trap.cpp"
#elif defined _WIN32
        #include "Trap.h"
#endif
#include "forces.cpp"
#include "calculators.cpp"
#include <iostream>
#include <cmath>
#include <fstream>
#include <map>
#include <string>
#include <time.h>
using namespace std;

//Here are the function definitions!
bool vectortest(void);
bool forcestest(void);


const double STABILITY_CRITERIA = 1E-5;
const int RESOLUTION = 40;
std::map<std::string, const char*> options;

// Option helpers
template <typename T> T option(const char* name, T dfault) {
	return options.count(name) > 0 ? T(options[name]) : dfault;
}
template <> int option<int>(const char* name, int dfault) {
	return options.count(name) > 0 ? std::atoi(options[name]) : dfault;
}
template <> double option<double>(const char* name, double dfault) {
	return options.count(name) > 0 ? std::atof(options[name]) : dfault;
}
template <> bool option<bool>(const char* name, bool dfault) {
	return options.count(name) > 0;
}

int main(int argc, const char* argv[]) {
        
        // Set random seed
        srand(time(NULL));

        // Naively read in options   
		std::pair<std::string, const char*>* opair = new std::pair<std::string, const char*>();
        for (int i = 0; i < argc; i++) {
                if (argv[i][0] == '-' && argv[i][1] == '-') {
					if (!opair->first.empty()) {
						opair->second = "";
						options.insert(*opair);
						delete opair;
						opair = new std::pair<std::string, const char*>();
					}
					opair->first = argv[i] + 2;

				} else if (!opair->first.empty()) {
                        opair->second = argv[i];
                        options.insert(*opair);
						delete opair;
						opair = new std::pair<std::string, const char *>();
                }
        }

		// Catch a final boolean option
		if (!opair->first.empty()) {
			opair->second = "";
            options.insert(*opair);
		}

        // Run self tests
        if (option<bool>("test", false)) {
                cout << boolalpha << "Running vector test, ";
                cout << vectortest() << endl << "Running forces test, ";
                cout << forcestest() << endl;
        }

        // Set-up our trap
        const double dt = 1E-9;
        Trap* t = new Trap(dt);
        
        ElectricField PenningE(option<double>("potential", 5E4));
        t->forces.push_back(&PenningE);
        MagneticField PenningM(0,0,1.75);
        t->forces.push_back(&PenningM);

        Laser DopplerC(option<double>("intensity", 100.0), 40E-6, 0, option<double>("offset", 20E-6), 0, 1, 0, 0);
        DopplerC.wavelength = 400E-9;
        DopplerC.detuning = option<double>("detuning", -50E6);
		DopplerC.saturation = option<double>("saturation", 1.0);
        t->forces.push_back(&DopplerC);

		Laser DopplerD(option<double>("intensity", 100.0), 40E-6, 0, 0, 0, 0, 0, 1);
		DopplerD.wavelength = 400E-9;
		DopplerD.detuning = option<double>("detuning", -50E6);
		DopplerD.saturation = 1.0;
		if (!option<bool>("flat", false)) t->forces.push_back(&DopplerD);

        const int PARTICLES = option<int>("particles", 1);
        for (int i = PARTICLES; i > 0; i--) {
                Particle* ion = new Particle;
                ion->mass = 6.69E-26;
                ion->charge = 1;
                t->Randomiser((*ion), option<double>("temperature", 5), option<bool>("flat", false));
                t->particles.push_back(ion);
                CoulombForce* c = new CoulombForce(ion);
                t->forces.push_back(c);

                cout << "Initial position (" << i << ") (x): " << ion->position.x << endl;
                cout << "Initial position (" << i << ") (y): " << ion->position.y << endl;
                cout << "Initial position (" << i << ") (z): " << ion->position.z << endl;
                cout << "Initial velocity (" << i << ") (x): " << ion->velocity.x << endl;
                cout << "Initial velocity (" << i << ") (y): " << ion->velocity.y << endl;
                cout << "Initial velocity (" << i << ") (z): " << ion->velocity.z << endl;
        }

        // Write out all relevant options
		if (option<bool>("alias", false)) cout << "Alias: " << option<const char*>("alias", "") << endl;
        cout << "Particles: " << PARTICLES  << endl;
        cout << "Electric field strength: " << PenningE.FieldStrength() << endl;
        cout << "Magnetic field strength: " << PenningM.FieldVector().z << endl;
        cout << "Laser wavelength: " << DopplerC.wavelength << endl;
        cout << "Laser detuning: " << DopplerC.detuning << endl;
        cout << "Laser position (x): " << DopplerC.position.x << endl;
        cout << "Laser position (y): " << DopplerC.position.y << endl;
        cout << "Laser position (z): " << DopplerC.position.z << endl;
        cout << "Laser direction (x): " << DopplerC.direction.x << endl;
        cout << "Laser direction (y): " << DopplerC.direction.y << endl;
        cout << "Laser direction (z): " << DopplerC.direction.z << endl;
        cout << "Laser beam width: " << DopplerC.width << endl;
        cout << "Laser intensity factor: " << DopplerC.fudgefactor << endl;
		cout << "Laser saturation: " << DopplerC.saturation << endl;
        cout << "Stability criteria: " << "Potential Energy v3 (Fourier transform)"<< endl;

        // Open file
        ofstream* output = new ofstream;
        const bool writeout = option<bool>("output", false);
        if (writeout) {
                output->open(option<const char*>("output", ""));
                cout << "Writing output data to " << option<const char*>("output", "") << endl;
        }

        // Initalise
        double time = 0, nexttime = RESOLUTION * dt;
        double mintime = option<double>("mintime", 0);
        double maxtime = option<double>("maxtime", 1);
        double average = 0;
        int i = 0;
        bool stable = false;
        bool takeinput = true;
        bool dotransform = false;
        int count = 0;
        bool first = true;
        
        const int bitno = 11; // Determines how many samples are being taken
        const int sampleno = pow(2.0,bitno);

        double * input = new double[sampleno];
        double * oldrealf = new double[sampleno];
        double * oldimgf = new double[sampleno];
        double * newrealf = new double[sampleno];
        double * newimgf = new double[sampleno];
        
        // The imaginary fourier components receive no input, so they are set to 0 just to be safe
        for(int k = 0; k < sampleno; k++){
        	oldimgf[k] = 0;
        	newimgf[k] = 0;
        }
        

        //while (time < 0.005) {
        while (time < maxtime && stable == false) {
                t->Simulate();
                time += dt;

                if (time > nexttime && time > mintime) {
                        nexttime = time + RESOLUTION * dt;

                        // Write out data if necessary
                        if (writeout) {
                                (*output) << time << "\t" << t->PotentialEnergy() << "\t";
                                for (std::vector<Particle*>::iterator i = t->particles.begin(); i != t->particles.end(); i++) {
                                        Particle ion = **i;
                                        (*output) << ion.position.x << "\t" << ion.position.y << "\t" << ion.position.z << "\t";
                                }
                                (*output) << "\n";
                        }
                        
                        if(count > 20000){
                        	takeinput = true;
                        }
                        
                        count++;
                        
                        // First, an input must be taken
                        if(takeinput == true){
                        	if(i<sampleno){
                        		input[i] = t->PotentialEnergy();
                        		i++;
                        	}
                        	else{
                        		takeinput = false;
                        		i = 0;
                        		dotransform = true;	
                        	}
                        }
                        
                        // After input is taken, fast fourier transform is done
                        if(dotransform == true){
                        	// First, bits are shuffled
                        	double * fourierreal;
                        	double * fourierimg;
                        	
                        	// If this is the first time a Fourier transform is performed, values are loaded into old
                        	// Otherwise, values are loaded into new
                        	if(first == true){
                        		fourierreal = oldrealf;
                        		fourierimg = oldimgf;
                        	}
                        	else{
                        		fourierreal = newrealf;
                        		fourierimg = newimgf;
                        	}
                        	
                        	// Bit shuffling is done
                        	for(int k = 0; k<sampleno; k++){
						int binno = 0;
						int temp = k;
							for(int l = 1; l <= bitno; l++){
								if(temp >= pow(2.0,bitno-l)){
									binno += pow(2.0,l-1);
									temp -= pow(2.0,bitno-l);
								}
							}
						fourierreal[binno] = input[k];
					}
					
					// Here comes funky FFT algorithm (took me ages to work out...)
					for(int k = 0; k < bitno; k++){
						int counter = 0;
						int twiddle = 0;
						bool done = false;
						for(int l = 0; l<sampleno; l++){
							if(done == false){
								int swapno = l+pow(2.0,k);
								double save0 = fourierreal[swapno];
								double save1;
								fourierreal[swapno] = fourierreal[swapno]*cos(2*PI*twiddle/pow(2.0,k+1))-fourierimg[swapno]*sin(2*PI*twiddle/pow(2.0,k+1));
								fourierimg[swapno] = save0*sin(2*PI*twiddle/pow(2.0,k+1)) + fourierimg[swapno]*cos(2*PI*twiddle/pow(2.0,k+1));
								twiddle = (twiddle + 1) % (swapno - l);
								
								save0 = fourierreal[l];
								save1 = fourierimg[l];
								
								fourierreal[l] = fourierreal[l] + fourierreal[swapno];
								fourierimg[l] = fourierimg[l] + fourierimg[swapno];
								
								fourierreal[swapno] = save0 - fourierreal[swapno];
								fourierimg[swapno] = save1 - fourierimg[swapno];							
							}
							
							counter++;
							if(counter == pow(2.0,k)){
								counter = 0;
								if(done == false){
									done = true;	
								}
								else{
									done = false;								
								}
							}
						}
					}
					
					// Here comes the bit where the amplitudes of the Fourier transform are being compared
					if(first == false){
						stable = true;
						double testpoten = t->PotentialEnergy()*sampleno/1000;
						for(int k = 0; k<sampleno; k++){
							double newamp = sqrt(pow(newrealf[k],2)+pow(newimgf[k],2));
							double oldamp = sqrt(pow(oldrealf[k],2)+pow(oldimgf[k],2));
							if(abs(newamp-oldamp) > testpoten){
								stable = false;
							}
							oldrealf[k] = newrealf[k];
							oldimgf[k] = newimgf[k];
							newrealf[k] = 0;
							newimgf[k] = 0;
						}
					}
					else{
						first = false;	
					}
					count = 0;
					dotransform = false;
					
                        }

                        // Stability test is now comparing amplitudes on Fourier transform

                        /*/ Check stability
                        if (time >= mintime) {
                                average = 0.0;
                                for (std::size_t i = 0; i < t.particles.size(); i++) {
                                        for (std::size_t j = i + 1; j < t.particles.size(); j++) {
                                                double seperation = (t.particles[i]->position - t.particles[j]->position).mag();
                                                average += seperation;
                                                seperations[i+j-1] = seperation;
                                        }
                                }
                                average /= seperations.size();
                                bool stable = true;
                                for (std::vector<double>::iterator d = seperations.begin(); d != seperations.end(); d++) {
                                        stable = stable && (*d < average * (1.0 + STABILITY_CRITERIA)) && (*d > average * (1.0 - STABILITY_CRITERIA));
                                }
                                if (stable) stabilityCount++;
                                if (!stable) stabilityCount = 0;
                        }*/
                }
        }
        
        delete input;
        delete oldrealf;
        delete oldimgf;
        delete newrealf;
        delete newimgf;
        

        cout << "Stable: " << (time >= maxtime ? "No" : "Yes") << endl;
        cout << "Time: " << time << endl;

        // Radius for triangles
        if (t->particles.size() == 3) {
                for (std::size_t i = 0; i < t->particles.size(); i++) {
                        for (std::size_t j = i + 1; j < t->particles.size(); j++) {
                                double seperation = (t->particles[i]->position - t->particles[j]->position).mag();
                                average += seperation;
                        }
                }
                average /= t->particles.size() * (t->particles.size() - 1) / 2;
                double radius = average / std::sqrt(3.0);
                cout << "Radius (assuming triangle): " << radius << endl;
                Particle* ion = t->particles[0];
                double emratio = ion->charge * 1.6E-19 / ion->mass, B = PenningM.FieldVector().z;
                cout << "Rotation speed (assuming triangle): " << sqrt(-emratio * (2 * PenningE.FieldStrength() - emratio * B * B / 4 + sqrt(3.0) * ion->charge * 1.6E-19 / (12 * PI * 8.85E-12 * pow(radius, 3)))) - emratio * B / 2 << endl;
        }

        if (writeout)
                (*output) << flush; // only flush at the end to save time
                
        delete output;

        // Calculate the centre of rotation
		std::vector<Calculator*> calculators;
		CentreOfRotationCalculator* cr = new CentreOfRotationCalculator();
		AverageFlourescenceCalculator* radial = new AverageFlourescenceCalculator(&DopplerC);
		AverageFlourescenceCalculator* axial = new AverageFlourescenceCalculator(&DopplerD);
		calculators.push_back(cr);
		calculators.push_back(radial);
		if (!option<bool>("flat", false)) calculators.push_back(axial);

        double period = t->OnePeriod(calculators);
		cout << "Rotation speed (from period): " << (2 * PI) / period << endl;

		vector3D<double> centre = cr->FinalResult();
        cout << "Center of rotation (x): " << centre.x << endl;
        cout << "Center of rotation (y): " << centre.y << endl;
        cout << "Center of rotation (z): " << centre.z << endl;
		cout << "Fluorescence (radial): " << radial->FinalResult() << endl;
		cout << "Fluorescence (axial): " << axial->FinalResult() << endl;
		cout << "Fluorescence (total): " << radial->FinalResult() + axial->FinalResult() << endl;
		
		// Measure the rotation speed and radius
		calculators.clear();
		RotationSpeedCalculator* rotator = new RotationSpeedCalculator(centre, dt);
		calculators.push_back(rotator);
		t->OnePeriod(calculators);
			
        average = 0.0;
        for (std::size_t i = 0; i < t->particles.size(); i++) {
                Particle* ion = t->particles[i];

                vector3D<double> relative = ion->position - centre;
				cout << "Relative position (" << i << ") (x): " << relative.x << endl;
                cout << "Relative position (" << i << ") (y): " << relative.y << endl;
                cout << "Relative position (" << i << ") (z): " << relative.z << endl;
                relative.z = 0;
                average += relative.mag();
                cout << "Radius (measured) (" << i << "): " << relative.mag() << endl;

                //ion->theta = relative.azimuth();
        }
        average /= t->particles.size();
        cout << "Radius (measured) (avg): " << average << endl;

        //t->Simulate();
        average = 0.0;
        for (std::size_t i = 0; i < t->particles.size(); i++) {
                Particle* ion = t->particles[i];
                //vector3D<double> relative = ion->position - centre;
                average += rotator->FinalResult(ion->id);
                cout << "Rotation speed (measured) (" << i << "): " << rotator->FinalResult(ion->id) << endl; //(relative.azimuth() - ion->theta)/dt << endl;
        }
        average /= t->particles.size();
        cout << "Rotation speed (measured) (avg): " << average << endl;
		
	delete t;
}

bool forcestest(void){

        //Here, a test particle is defined to test the forces program
        Particle ion;
        ion.velocity = vector3D<double>(3,4,0);
        ion.position = vector3D<double>(2,3,0);
        ion.mass = 6.69E-26;
        ion.charge = 1;

        //Electric and magnetic fields are initialised
        ElectricField PenningE(5E4);
        MagneticField PenningM(0,0,1.75);

        //The forces are applied to the particle and added up
        vector3D<double> resultantforce;
        resultantforce = PenningE.ApplyTo(ion) + PenningM.ApplyTo(ion);
        
        //Forces are displayed in the terminal
        cout << "Resultant force:" << endl << "x = " << resultantforce.x << endl << "y = " << resultantforce.y << endl << "z = " << resultantforce.z << endl;

        /*// Laser testing
        Laser argh(exp(1.0),1,vector3D<double>(2,2,0),vector3D<double>(0,0,1));
        argh.wavelength = 600.0;
        cout << "Laser intensity at centre = " << argh.intensity << endl << "Laser intensity at point (" << ion.position.x << "," << ion.position.y << "," << ion.position.z << ") = " << argh.IntensityatPoint(ion.position) << endl;
        
        // Doppler testing
        cout << "Doppler shift from 600 nm wavelength when moving towards source with 2000 metres per second = " << argh.DopplerWavelength(vector3D<double>(0,0,-2000)) << endl;
        */
        return true;
}




bool vectortest() {
        vector3D<double> vec = vector3D<double>(3,4,0);
        vector3D<double> hey(2,4,3);

        //I worry a bit about rounding errors with these tests, we might flag false mistakes
        if(vec*hey!=22){
                return false;
        }
        
        else if( (vec^hey).x != 12 || (vec^hey).y != -9 || (vec^hey).z != 4 ){
                return false;
        }

        else{
                return true;
        }
}
