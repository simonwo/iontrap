#pragma once
#include <cmath>

template <class T>
class vector3D {

public:
	T x;
	T y;
	T z;
	vector3D();
	vector3D(T x, T y, T z);

	const bool operator==(const vector3D<T>&) const;
	const bool operator!=(const vector3D<T>&) const;

	const vector3D operator+(const vector3D<T>&) const;
	vector3D & operator+=(const vector3D<T>&);
	const vector3D operator-(const vector3D<T>&) const;
	vector3D & operator-=(const vector3D<T>&);
	const vector3D operator*(const T) const; // i don't really understand this
	vector3D & operator*=(const T); 
	const vector3D operator/(const T) const;
	vector3D & operator/=(const T);

	const T operator*(const vector3D<T>&);
	const vector3D operator^(const vector3D<T>&) const;

	const T mag(void) const;
	const T azimuth(void) const;
	const T radius(void) const;
};

template <class T>
vector3D<T>::vector3D() : x(0), y(0), z(0) { };

template <class T>
vector3D<T>::vector3D(T x, T y, T z) : x(x), y(y), z(z) { };

template <class T>
const inline bool vector3D<T>::operator==(const vector3D<T>& V) const {
	return (this->x == V.x) && (this->y == V.y) && (this->z == V.z);
}

template <class T>
const inline bool vector3D<T>::operator!=(const vector3D<T>& V) const {
	return !(*this == V);
}

template <class T>
const inline vector3D<T> vector3D<T>::operator+(const vector3D<T>& V) const {
	return vector3D<T>( this->x + V.x, this->y + V.y, this->z + V.z);
};

template <class T>
inline vector3D<T> & vector3D<T>::operator+=(const vector3D<T>& V){
	this->x += V.x;
	this->y += V.y;
	this->z += V.z;
	return *this;
};

template <class T>
const inline vector3D<T> vector3D<T>::operator-(const vector3D<T>& V) const {
	return vector3D<T>( this->x - V.x, this->y - V.y, this->z - V.z);
};

template <class T>
inline vector3D<T> & vector3D<T>::operator-=(const vector3D<T>& V){
	this->x -= V.x;
	this->y -= V.y;
	this->z -= V.z;
	return this;
};

template <class T>
const inline vector3D<T> vector3D<T>::operator*(const T S) const {
	return vector3D<T>( this->x * S, this->y * S, this->z * S);
};

template <class T>
inline vector3D<T> & vector3D<T>::operator*=(const T S){
	this->x *= S;
	this->y *= S;
	this->z *= S;
	return this;
};

template <class T>
const inline T vector3D<T>::operator*(const vector3D<T>& V) {
	return this->x * V.x + this->y * V.y + this->z * V.z;
};

template <class T>
const inline vector3D<T> vector3D<T>::operator/(const T S) const {
	return vector3D<T>( this->x / S, this->y / S, this->z / S);
};

template <class T>
inline vector3D<T> & vector3D<T>::operator/=(const T S){
	this->x /= S;
	this->y /= S;
	this->z /= S;
	return this;
};

template <class T>
const inline vector3D<T> vector3D<T>::operator^(const vector3D<T>& V) const {
	return vector3D<T>( this->y * V.z - V.y * this->z,
						V.x * this->z - this->x * V.z,
						this->x * V.y - V.x * this->y  );
};


template <class T>
const inline T vector3D<T>::mag(void) const {
	return sqrt(x*x+y*y+z*z);
}

template <class T>
const inline T vector3D<T>::radius(void) const {
	return sqrt(x*x+y*y);
}

template <class T>
const inline T vector3D<T>::azimuth(void) const {
	return atan2(y, x);
}