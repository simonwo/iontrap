<?php 

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	if (array_key_exists("Magic_word", $_POST) && strcmp($_POST['Magic_word'], "a5prus10") == 0) {
		$db = new mysqli("localhost", "iontrap", "CD285pbtlJQdVc1", "client86077_iontrap");
		
		// Fetch new job id
		$result = $db->query("SELECT IFNULL( MAX( jobid ) , 0 ) +1 AS NewID FROM `rundata`");
		$jobid = $result->fetch_assoc();
		$jobid = $jobid['NewID'];
		
		$stmt = $db->prepare("INSERT INTO rundata (jobid, name, value) VALUES (?, ?, ?)");
		$stmt->bind_param('dss', $jobid, $name, $value);

		foreach ($_POST as $name => $value) {
			if ($name == "Magic_word") continue;
			$name = str_replace("_", " ", $name);
			$stmt->execute();
		}
	} else {
		header("HTTP/1.1 403 Forbidden");
	}
} else {
	// Output all data
	header("Content-Disposition: attachment; filename=\"data.csv\"");
	header("Pragma: public");
	header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	header("Content-Type: text/csv");
	
	$db = new mysqli("localhost", "iontrap", "CD285pbtlJQdVc1", "client86077_iontrap");

	$stmt = "SELECT "; 
	$result = $db->query("SELECT DISTINCT name FROM rundata ORDER BY name ASC"); 
	$names = array();
	while ($row = $result->fetch_assoc()) {
		$names[] = $row['name'];
		$stmt .= "GROUP_CONCAT(IF(name = '" . $row['name'] . "', value, NULL)) AS '" . $row['name'] . "', ";
		echo $row['name'], ", ";
	} 
	$stmt .= "jobid FROM rundata GROUP BY jobid";
	echo "\n";
	
	$result = $db->query($stmt);
	while ($row = $result->fetch_assoc()) {
		foreach ($names as $name) {
			echo trim($row[$name]), ", ";
		}
		echo "\n";
	}
}
?>